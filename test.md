# Team Onboarding Checklist v.2

1. **Before meeting a team**
    - [x] Register the team in MWA Profilers
    - [x] Send MWA Team questionnaire 
2. **First touch with the team**
    - [x] Invite to the introduction meeting TTL, TL, PO, BA
    - [ ] Provide the 30m meeting. The sense of the meeting is to let the team introduce themself and introduce to the team an MWA coaches
    - [ ] Create an official channel for the team (later channel members will be increasing by demand)
    - [ ] Send a list of relevant material to the team
    - [x] Request team to come up with MWA candidates (Candidates need to have business requirements)
